package com.havenuidea.graph;

import java.util.*;
import java.util.stream.Collectors;

public class Graph<T> {

    private final Set<T> vertices;
    private final Map<T, Set<Edge<T>>> edges;
    private final boolean directed;

    public Graph(boolean directed) {
        this.vertices = new HashSet<>();
        this.edges = new HashMap<>();
        this.directed = directed;
    }

    public void addVertex(T newVertex) {
        vertices.add(newVertex);
    }

    public void addEdge(Edge<T> newEdge) {
        T from = newEdge.getFrom();
        T to = newEdge.getTo();
        vertices.add(from);
        vertices.add(to);
        Set<Edge<T>> fromEdges = this.edges.computeIfAbsent(from, (s) -> new HashSet<>());
        fromEdges.add(newEdge);
        if (!directed) {
            Set<Edge<T>> toEdges = this.edges.computeIfAbsent(to, (s) -> new HashSet<>());
            toEdges.add(newEdge);
        }
    }

    public List<Edge<T>> getPath(T start, T finish) {
        validateInput(start);
        validateInput(finish);

        Set<Edge<T>> edges = this.edges.getOrDefault(start, new HashSet<>());
        Deque<List<Edge<T>>> paths = new LinkedList<>();
        edges.forEach(edge -> {
            List<Edge<T>> list = new LinkedList<>();
            list.add(edge);
            paths.add(list);
        });
        Set<T> processed = new HashSet<>();
        processed.add(start);
        while (!paths.isEmpty()) {
            List<Edge<T>> path = paths.pollFirst();
            int size = path.size();
            Edge<T> lastEdge = path.get(size - 1);
            T other = lastEdge.getOtherVertex(processed);
            if (finish.equals(other)) {
                return path;
            }
            if (processed.add(other)) {
                List<Edge<T>> newEdgesToExplore = this.edges.getOrDefault(other, new HashSet<>()).stream()
                        .filter(edge -> !edge.equals(lastEdge))
                        .collect(Collectors.toList());
                newEdgesToExplore.forEach(edge -> {
                    List<Edge<T>> list = new LinkedList<>(path);
                    list.add(edge);
                    paths.add(list);
                });
            }

        }
        throw new IllegalArgumentException("No path found");
    }

    private void validateInput(T vertex) {
        Objects.requireNonNull(vertex);
        if (!vertices.contains(vertex)) {
            throw new IllegalArgumentException("no such vertex found in graph " + vertex);
        }
    }
}
