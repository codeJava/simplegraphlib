package com.havenuidea.graph;

import java.util.Objects;
import java.util.Set;

public class Edge<T> {

    private T from;
    private T to;

    public Edge(T from, T to) {
        this.from = from;
        this.to = to;
    }

    public T getFrom() {
        return from;
    }

    public T getTo() {
        return to;
    }

    @Override
    public String toString() {
        return from + " <-> " + to;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Edge edge = (Edge) o;
        return Objects.equals(from, edge.from) &&
                Objects.equals(to, edge.to);
    }

    @Override
    public int hashCode() {
        return Objects.hash(from, to);
    }

    T getOtherVertex(Set<T> excludeVertices) {
        if (!excludeVertices.contains(getFrom())
                && !excludeVertices.contains(getTo())) {
            throw new IllegalStateException("edge has no excluded vertices");
        }
        if (excludeVertices.contains(getFrom())) {
            return getTo();
        } else {
            return getFrom();
        }
    }
}
