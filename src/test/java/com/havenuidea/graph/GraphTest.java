package com.havenuidea.graph;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@RunWith(Parameterized.class)
public class GraphTest {

    @Parameters
    public static Collection<Object[]> data() {
        List<Object[]> params = new ArrayList<>();
        params.add(new Object[]{false});
        params.add(new Object[]{true});
        return params;
    }

    private boolean directed;

    public GraphTest(boolean directed) {
        this.directed = directed;
    }

    @Test
    public void testOneEdgeGraph() {
        Graph<String> g = new Graph<>(directed);
        String v1 = "Start";
        String v2 = "End";
        Edge<String> edge = new Edge<>(v1, v2);
        g.addEdge(edge);
        List<Edge<String>> path = g.getPath(v1, v2);
        assertEquals(1, path.size());
        assertEquals(edge, path.get(0));
    }

    @Test
    public void testTwoEdgeGraph() {
        Graph<String> g = new Graph<>(directed);
        String start = "Start";
        String middle = "Middle";
        String end = "End";
        Edge<String> edge = new Edge<>(start, middle);
        g.addEdge(edge);
        Edge<String> edge1 = new Edge<>(end, middle);
        g.addEdge(edge1);
        try {
            List<Edge<String>> path = g.getPath(start, end);
            assertEquals(2, path.size());
            assertEquals(edge, path.get(0));
            assertEquals(edge1, path.get(1));
        } catch (IllegalArgumentException e) {
            if (!directed) {
                fail();
            } else {
                assertEquals(e.getMessage(), "No path found");
            }
        }
    }

    @Test
    public void testSeveralBranchesGraph() {
        Graph<String> g = new Graph<>(directed);
        String start = "Start";
        String middle = "Middle";
        String middle2 = "Middle2";
        String end2 = "end2";
        String middle3 = "Middle3";
        String end3 = "end3";
        String end4 = "end4";
        String end5 = "end5";
        String end = "End";
        Edge<String> startMid = new Edge<>(start, middle);
        g.addEdge(startMid);
        Edge<String> midEnd = new Edge<>(middle, end);
        g.addEdge(midEnd);
        g.addEdge(new Edge<>(middle, end2));
        g.addEdge(new Edge<>(start, middle2));
        g.addEdge(new Edge<>(start, middle3));
        g.addEdge(new Edge<>(middle, end2));
        g.addEdge(new Edge<>(middle, end3));
        g.addEdge(new Edge<>(middle, end5));
        g.addEdge(new Edge<>(middle2, end2));
        g.addEdge(new Edge<>(middle2, end4));

        List<Edge<String>> path = g.getPath(start, end);
        assertEquals(2, path.size());
        assertEquals(startMid, path.get(0));
        assertEquals(midEnd, path.get(1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNoPathGraph() {
        Graph<String> g = new Graph<>(directed);
        String start = "Start";
        String end = "End";
        String middle = "Middle";
        Edge<String> edge = new Edge<>(start, middle);
        g.addEdge(edge);
        g.addVertex(end);
        List<Edge<String>> path = g.getPath(start, end);
        assertEquals(1, path.size());
        assertEquals(edge, path.get(0));
    }

    @Test
    public void testBiggerSeveralBranchesGraph() {
        Graph<String> g = new Graph<>(directed);
        String start = "Start";
        String middle = "Middle";
        String middle2 = "Middle2";
        String end2 = "end2";
        String middle3 = "Middle3";
        String end3 = "end3";
        String end4 = "end4";
        String end5 = "end5";
        String end = "End";


        g.addEdge(new Edge<>(start, middle));
        g.addEdge(new Edge<>(middle, end));
        g.addEdge(new Edge<>(middle, end2));
        g.addEdge(new Edge<>(start, middle2));
        Edge<String> m2m3 = new Edge<>(middle2, middle3);
        g.addEdge(m2m3);
        g.addEdge(new Edge<>(middle, end2));
        g.addEdge(new Edge<>(middle3, end3));
        Edge<String> m3e5 = new Edge<>(middle3, end5);
        g.addEdge(m3e5);
        g.addEdge(new Edge<>(middle2, end2));
        Edge<String> m2e4 = new Edge<>(middle2, end4);
        g.addEdge(m2e4);

        try {
            List<Edge<String>> path = g.getPath(end4, end5);
            assertEquals(3, path.size());
            assertEquals(m2e4, path.get(0));
            assertEquals(m2m3, path.get(1));
            assertEquals(m3e5, path.get(2));
        } catch (IllegalArgumentException e) {
            if (!directed) {
                fail();
            } else {
                assertEquals(e.getMessage(), "No path found");
            }
        }
    }

    @Test
    public void testCycledGraph() {
        Graph<String> g = new Graph<>(directed);
        String v1 = "1";
        String v2 = "2";
        String v3 = "3";
        String v4 = "4";
        String v5 = "5";
        String v6 = "6";
        String v7 = "7";
        String v8 = "8";
        String v9 = "9";

        g.addEdge(new Edge<>(v2, v1));
        g.addEdge(new Edge<>(v2, v9));
        g.addEdge(new Edge<>(v1, v3));
        g.addEdge(new Edge<>(v3, v5));
        g.addEdge(new Edge<>(v4, v2));
        g.addEdge(new Edge<>(v5, v6));
        g.addEdge(new Edge<>(v5, v8));
        g.addEdge(new Edge<>(v3, v7));
        g.addEdge(new Edge<>(v1, v7));
        g.addEdge(new Edge<>(v9, v4));

        List<Edge<String>> path = g.getPath(v4, v8);
        assertEquals(5, path.size());
    }

    @Test
    public void testRandomGraph() {
        List<Edge<String>> path = generateRandomPath("", 1);
        String start = path.get(0).getFrom();
        String end = path.get(path.size() - 1).getTo();
        Graph<String> g = new Graph<>(directed);
        Set<String> vertices = new HashSet<>();
        for (Edge<String> edge : path) {
            g.addEdge(edge);
            vertices.add(edge.getFrom());
            vertices.add(edge.getTo());
        }
        List<Edge<String>> edges = generateRandomEdges(vertices);
        for (Edge<String> edge : edges) {
            g.addEdge(edge);
        }
        List<Edge<String>> result = g.getPath(start, end);
        assertEquals(path.size(), result.size());
        for (int i = 0; i < path.size(); i++) {
            assertEquals(path.get(i), result.get(i));
        }
    }

    private List<Edge<String>> generateRandomPath(String prefix, long minLength) {
        List<Edge<String>> path = new ArrayList<>();
        long numberOfEdges = minLength + Math.round(Math.random() * 50);
        for (int i = 0; i < numberOfEdges; i++) {
            Edge<String> edge = new Edge<>(prefix + i, prefix + (i + 1));
            path.add(edge);
        }
        return path;
    }

    private List<Edge<String>> generateRandomEdges(Set<String> vertexSet) {
        List<Edge<String>> results = new ArrayList<>();
        List<String> vertexList = new ArrayList<>(vertexSet);
        int initialPathSize = vertexList.size();
        while (vertexList.size() > 1) {
            int index = Math.toIntExact(Math.round(Math.random() * (vertexList.size() - 1)));
            int linkedIndex = Math.toIntExact(Math.round(Math.random() * (vertexList.size() - 2)));
            String v = vertexList.remove(index);
            List<Edge<String>> edges = generateRandomPath(v + "_", initialPathSize + 1);
            edges.add(0, new Edge<>(v, edges.get(0).getFrom()));
            edges.add(new Edge<>(edges.get(edges.size() - 1).getTo(), vertexList.get(linkedIndex)));
            results.addAll(edges);
        }
        return results;
    }
}
